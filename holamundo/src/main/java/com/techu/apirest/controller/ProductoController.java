package com.techu.apirest.controller;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/products")
    public List<ProductoModel> getProducts(){
        return productoService.findAll();
    }

    @GetMapping("/products/{id}")
    public Optional<ProductoModel> getProductById(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/products")
    public ProductoModel postProduct(@RequestBody ProductoModel newProduct){
        try {
            return productoService.save(newProduct);
        } catch (Exception e){
            return null;
        }
    }

    @PutMapping("/products/{id}")
    public void putProduct(@RequestBody ProductoModel updatedProduct, @PathVariable String id){
        if (productoService.existsById(id)) {
            productoService.save(updatedProduct);
        }
    }

    @DeleteMapping("/products/{id}")
    public boolean deleteProduct(@PathVariable String id){//, @RequestBody ProductoModel productToDelete) {
        if (productoService.existsById(id)) {
            ProductoModel productToDelete = productoService.findById(id).get();
            productoService.isDeleted(productToDelete);
            return true;
        } else
            return false;
    }

    @PatchMapping("/products/{id}")
    public ProductoModel patchProduct(@PathVariable String id, @RequestBody ProductoModel patchedProduct) {
        if(productoService.findById(id).isPresent() == true) {
            ProductoModel productToPatch = productoService.findById(id).get();
            if(patchedProduct.getPrice() != null) {
                productToPatch.setPrice(patchedProduct.getPrice());
            }
            if (patchedProduct.getDescription() != null) {
                productToPatch.setDescription(patchedProduct.getDescription());
            }
            return productoService.save(productToPatch);
        }
        return null;
    }
    /*
    //ejemplo de otro alumno
    @PatchMapping(BASEURL + "/products/{id}")
    public ProductoModel patchProducts(@RequestBody ProductoModel productToPatch, @PathVariable int id) {
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).getId() == id) {
                if (productToPatch.getId() != null) {
                    products.get(i).setId(productToPatch.getId());
                }
                if (productToPatch.getDescription() != null) {
                    products.get(i).setDescription(productToPatch.getDescription());
                }
                if (productToPatch.getPrice() != null) {
                    products.get(i).setPrice(productToPatch.getPrice());
                }
                return products.get(i);
            }
        }
        ProductoModel noProducto = new ProductoModel(0, "null", 0.0);
        return noProducto;
    }

    /* ejemplo ezequiel
    @PatchMapping(BASEURL + "/productos/{id}")
    public ProductoModel patchProductos(@RequestBody ProductoModel productoToUpdate, @PathVariable int id){
        for(int i=0;i<productos.size();i++){
            if(productos.get(i).getId() == id) {
                productos.set(i, productoToUpdate);
                return productos.get(i);
            }
        }
        return (new ProductoModel(0, "null", 0.0));
    }
     */
}
